const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();

const app = express();

app.use(bodyParser.json());

app.listen(9000, () => {
  console.log('XNAT Metrics Reports Collector running on port 9000 ');
});

app.get('/', async (req, res) => {
  res.status(200);
  res.send('XNAT Metrics Report collector is working');
});

app.post('/', async (req, res) => {
  res.status(200);
  res.send('Report Received');
  console.log(req.body);
});

app.post('/401', async (req, res) => {
  res.status(401).json({
    status: 401,
    message: 'Must be authenticated to access the XNAT REST API'
  });
});

app.post('/403', async (req, res) => {
  res.status(403).json({
    status: 403,
    message: 'Not authorized to set the site configuration properties'
  });
});

app.post('/404', async (req, res) => {
  res.status(404).json({ status: 404, message: 'Not Found' });
});

app.post('/500', async (req, res) => {
  res.status(500).json({ status: 500, message: 'Unexpected Error' });
});
