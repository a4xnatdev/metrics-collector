# XNAT Metrics Report Collector

This web application acts as a dummy endpoint to send your reports. 
The web application is a smiple NodeJS application with end points to test various status repsonses.
The application supports following status codes
200 - Report Received
401 - Must be authenticated to access the XNAT REST API
403 - Not authorized to set the site configuration properties
404 - Not Found
500 - Unexpected Error

## Getting Started

You will need to first create a docker image by running following command

```
# Navigate to downloaded metrics-collector folder
cd metrics-collector

# Create docker image
docker build -t metrics-collector .
```

If the command runs successfully, check if the image is created by running following command

```
docker image ls
```

You should see ```metrics-collector``` image in the list

Execute following command to run the image

```
docker run -p 9000:9000 metrics-collector
```

Following are the urls to get the responses

GET     http://localhost:9000       - Will return message XNAT Metrics Report collector is working"

POST    http://localhost:9000/      - Will return status code 200 and body of request will printed on screen

POST    http://localhost:9000/401   - Will return status code 401 and message "Must be authenticated to access the XNAT REST API"

POST    http://localhost:9000/403    - Will return status code 403 and message "Not authorized to set the site configuration properties"

POST    http://localhost:9000/404	- Will return status code 404 and meesage "Not Found"

POST	http://localhost:9000/500	- Will return status code 500 and message "Unexpected Error"